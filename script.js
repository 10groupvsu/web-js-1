  window.onload = init;
    let mas = [];       //массив с введёнными празд днями
    let i=-1;
    let masNoWork = [];  //массив нерабочих дней в месяце

     //инициализация
    function init()
    {
      var solveButton = document.getElementById("solveButton");
      solveButton.onclick = solveButtonClick;

      var doButton = document.getElementById("doButton");
      doButton.onclick = doButtonClick;

    }


 //нажатие кнопки вычислить
     function doButtonClick()
    {
        var month_year = document.getElementById("month_year").value;
	month_year=month_year+"-01";

	var start = new Date(month_year); //дата начала месяца
	start.setHours(0, 0, 0, 0);//сбрасываем часы
	var end = new Date( start.getFullYear(), start.getMonth() + 1, 0); //дата конца месяца
	end.setHours(0, 0, 0, 0);


       let j=-1;

	// перебираем все дни месяца, проверяем которые из них выходные-суббота и воскресение
     
	for (var d = start; d <= end; d.setDate(d.getDate() + 1)) 
	{
		if (d.getDay()==0 || d.getDay()==6)
		{  
			j++;
			masNoWork[j]=new Date(d);  //добавляем выходной день в массив нерабочих дней
		}
	}
	
       //перебираем массив с праздниками	
	var index;
	for (index = 0; index < mas.length; ++index) 
	{
	  var prazd=new Date(mas[index]);
	 
          if (prazd.getDay()>0 && prazd.getDay()<=6)  // если праздник выпал от понедельника до субботы
		{
                        //проверим есть ли уже эта дата в массиве нерабочих дней ?
			while (prazd.getDate()!=1)           //цикл повторяем до 1 дня
			{   
 			   if (CheckInMas2(prazd, masNoWork) == false) //если такой даты нет
				{
				  j++;  
				  masNoWork[j]=prazd;                   //сохраняем её в массиве нерабочих дней
				  break;	                        // и сразу выходим из цикла
				}
	                   prazd.setDate(prazd.getDate()-1);  //если такая дата уже есть, значит уменьшаем дату на 1
			}
		}
	}
      

      //Выводим отчёт
      var n_days = end.getDate();      //количество дней в месяце
      var len_mas=masNoWork.length ;   //количество нерабочих дней    
 
     var firstDay=new Date(end.getFullYear(),end.getMonth(), 15); //начнём с 15-го числа
     while (CheckInMas2(firstDay, masNoWork) == true) firstDay.setDate(firstDay.getDate()-1);  //начиная с 15-го числа ищем свободную дату для 1-й выплаты
     while (CheckInMas2(end, masNoWork) == true)  end.setDate(end.getDate()-1);       //начиная с последнего дня месяца ищем свободную дату для 2-й выплаты

     alert("количество рабочих дней:"+(n_days-len_mas)
		+"\n отработанно часов:"+8*(n_days-len_mas)
		+"\n 1 дата выплат:"+firstDay.toLocaleDateString()
		+"\n 2 дата выплат:"+end.toLocaleDateString());
    }



     //функция проверяет есть ли дата d в массиве arr
    function CheckInMas2(d, arr)
     {
	var index;
	for (index = 0; index < arr.length; ++index) 
 	 {
             var prazd=new Date(arr[index]);

   	     if (prazd.getFullYear()==d.getFullYear() 
		&& prazd.getMonth()==d.getMonth()
		&& prazd.getDate()==d.getDate() )
		{ return true;
		}
 	 }
	return false;	
      }



    //ввод данных
   
   // нажатие кнопки - добавить праздник 
    function solveButtonClick()
    {

      var month_year = document.getElementById("month_year").value;
      month_year=month_year+"-01";
      var holiday = document.getElementById("holiday").value;


      if(holiday=="")
       {
        alert(" Не ввели дату праздника!" );
        return ;
       }


       var m_y = new Date(month_year);
       var dat = new Date(holiday);

       if (m_y.getFullYear()!=dat.getFullYear() || m_y.getMonth()!=dat.getMonth() )
	{
	 alert("Ошибка! Месяца не совпадают!" );	
  	 return;
	}

       if (  CheckInMas2(dat, mas) ==true)
	{
	 alert("Ошибка! такая дата уже введена!" );	
  	 return;
	}


       if(i==4)
        {
          alert("Праздник не добавлен! Не может быть больше 5 праздников в месяце!" );
          return ;
        }

       //если всё хорошо, добавляем дату в массив
         i++;
         mas[i]=dat;
         alert("Праздник добавлен." );
     }
